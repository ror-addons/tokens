<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="Tokens" version="1.0" date="6/08/2010" >
		<Author name="Ayonyx" email="ayonyx@ayonyx.net" />
		<Description text="Currency Token Display." />
		<VersionSettings gameVersion="1.3.5" windowsVersion="1.0" savedVariablesVersion="1.0" />
		
		<Dependencies>
			<Dependency name="EA_BackpackWindow" />
		</Dependencies>

		<Files>
			<File name="Libs/LibStub.lua" />
			<File name="Libs/LibGUI.lua" />
			<File name="core.lua" />
		</Files>

		<OnInitialize>
			<CallFunction name="Tokens.Initialize" />
		</OnInitialize>
		
		<OnUpdate/>
		
		<OnShutdown/>
		
		<SavedVariables>
			<SavedVariable name="Tokens.Save" />
		</SavedVariables>
		
		<WARInfo>
			<Categories>
				<Category name="INVENTORY" />
			</Categories>
			<Careers>
				<Career name="BLACKGUARD" />
				<Career name="WITCH_ELF" />
				<Career name="DISCIPLE" />
				<Career name="SORCERER" />
				<Career name="IRON_BREAKER" />
				<Career name="SLAYER" />
				<Career name="RUNE_PRIEST" />
				<Career name="ENGINEER" />
				<Career name="BLACK_ORC" />
				<Career name="CHOPPA" />
				<Career name="SHAMAN" />
				<Career name="SQUIG_HERDER" />
				<Career name="WITCH_HUNTER" />
				<Career name="KNIGHT" />
				<Career name="BRIGHT_WIZARD" />
				<Career name="WARRIOR_PRIEST" />
				<Career name="CHOSEN" />
				<Career name= "MARAUDER" />
				<Career name="ZEALOT" />
				<Career name="MAGUS" />
				<Career name="SWORDMASTER" />
				<Career name="SHADOW_WARRIOR" />
				<Career name="WHITE_LION" />
				<Career name="ARCHMAGE" />
			</Careers>
		</WARInfo>
	</UiMod>
</ModuleFile>
