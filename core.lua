--SystemData.Events.PLAYER_CURRENCY_SLOT_UPDATED
--EA_Window_Backpack.TYPE_CURRENCY
Tokens = {}
local numWindows = 0
local GUI = LibStub("LibGUI")

local availTrackers = {}
local usedTrackers = {}
--[[------------------------------------------------------
-- GetTokenData
-- expects: 
	* itemID - int 
-- returns:
	* stackCount - number of tokens
	* iconNum 	 - icon number of token
------------------------------------------------------]]--
local function GetTokenData(itemID)
	d("GetTokenData("..itemID..")")
	local data = DataUtils.GetCurrencyItems()
	
	for _,v in ipairs(data) do
		if(v.uniqueID == itemID) then
			return v.stackCount, v.iconNum
		end
	end
	return 0, 0
end

--[[------------------------------------------------------
-- Spawns a new TokenView
------------------------------------------------------]]--
local function CreateNewTokenView()
	local W
	
	local name = "Tokens_"..numWindows + 1
	
	W = GUI("Window", name,"TooltipBase")
	W:Resize(200, 40)
	
	W.Icon = W("Image")
	W.Icon:Resize(40, 40)
	W.Icon:TexDims(64, 64)
	W.Icon:AnchorTo(W, "left", "left", 10, 0)
	W.Icon:Layer("default")
	W.Icon:IgnoreInput()
	
	W.Count = W("Label")
	W.Count:Resize(160)
	W.Count:AnchorTo(W.Icon, "left", "left", 0, 0)
	W.Count:Font("font_clear_medium_bold")
	W.Count:Align("center")
	W.Count:Layer("secondary")
	W.Count:IgnoreInput()
	
	W.Delete = W("closebutton")
	W.Delete:AnchorTo(W, "right", "right", 0, 0)
	
	W.Delete.OnLButtonUp = function()
        Tokens.RemTracker(W.ID)
	end
    
	
	numWindows = numWindows + 1
	
	return W
end

--[[------------------------------------------------------
-- GetTracker
-- expects:
	* nothing
-- returns
	* tracker

-- returns a display object for a new token tracker
------------------------------------------------------]]--
local function GetTracker(id)
	local t = table.remove(availTrackers)
	
	if not t then 
		t = CreateNewTokenView()
	end
	
	t.ID = id
	
	return t
end


-- initialize
function Tokens.Initialize()
	Tokens.Save = Tokens.Save or {}
	Tokens.Save.tracked = Tokens.Save.tracked or {}
	
	RegisterEventHandler(SystemData.Events.PLAYER_CURRENCY_SLOT_UPDATED, "Tokens.SlotUpdated")
	
	Tokens.RegisterSlash()
	
	Tokens.CreateAnchor()
	
	for k in pairs(Tokens.Save.tracked) do 
		Tokens.SpawnTracker(k)
	end
	
	 Tokens.OrderTrackers()
end

--[[------------------------------------------------------
-- CreateAnchor
-- expects:
	* nothing
-- returns
	* nothing

-- creates the default anchor for all trackers and
-- restores it's position
------------------------------------------------------]]--
function Tokens.CreateAnchor()
	W = GUI("Window", "TokensAnchor", "TooltipBase")
	W:Resize(200, 40)
	
	LayoutEditor.RegisterWindow(W.name,L"Tokens","", false,false,false)
	table.insert(LayoutEditor.EventHandlers, Tokens.SavePosition)
	
	local scale = InterfaceCore.GetScale()
	
	local x = Tokens.Save.x or 1
	local y = Tokens.Save.y or 1
	
	W:Position(x/scale, y/scale)
end

--[[------------------------------------------------------
-- SavePosition
-- expects:
	* nothing
-- returns
	* nothing

-- saves the position of the anchor when the layout editor
-- is saved
------------------------------------------------------]]--
function Tokens.SavePosition()
	if not DoesWindowExist("TokensAnchor") then return end
	
	local x,y = WindowGetScreenPosition("TokensAnchor")
	
	Tokens.Save.x = x
	Tokens.Save.y = y
end

--[[------------------------------------------------------
-- SpawnTracker
-- expects:
	* itemID - int
-- returns:
	* nothing

-- creates trackers for anything that should already have
-- a tracker.
------------------------------------------------------]]--
function Tokens.SpawnTracker(itemID)
	usedTrackers[itemID] = GetTracker(itemID)
	usedTrackers[itemID].Icon:Texture(GetIconData(Tokens.Save.tracked[itemID].icon))
	usedTrackers[itemID].Count:SetText(wstring.format(L"%d/%d", Tokens.Save.tracked[itemID].current, Tokens.Save.tracked[itemID].goal))
end

--[[------------------------------------------------------
-- SlotUpdated
-- expects: 
	* updatedSlots - table
-- returns:
	* nothing

-- called whenever a token slot is updated, expected
-- variables are automatically passed to the function
------------------------------------------------------]]--
function Tokens.SlotUpdated(updatedSlots)
	for _, v in ipairs(updatedSlots) do
	
		if(slot == 0) then continue end
		
		local itemData = EA_Window_Backpack.GetItemsFromBackpack(EA_Window_Backpack.TYPE_CURRENCY)[v]
	
		if(Tokens.Save.tracked[itemData.uniqueID]) then
			Tokens.UpdateStackCount(itemData)
		end
	end
end

--[[------------------------------------------------------
-- UpdateStackCount
-- expects: 
	* itemData - table
-- returns:
	* nothing
	
-- if a token is being tracked, then this function is
-- passed the item data when it's been updated in order
-- to update current number of tokens.
------------------------------------------------------]]--
function Tokens.UpdateStackCount(itemData)
	Tokens.Save.tracked[itemData.uniqueID].current = itemData.stackCount
	
	if Tokens.Save.tracked[itemData.uniqueID].icon == 0 then
		Tokens.Save.tracked[itemData.uniqueID].icon = itemData.iconNum
		usedTrackers[itemData.uniqueID].Icon:Texture(GetIconData(Tokens.Save.tracked[itemData.uniqueID].icon))
	end
	
	usedTrackers[itemData.uniqueID].Count:SetText(wstring.format(L"%d/%d", Tokens.Save.tracked[itemData.uniqueID].current, Tokens.Save.tracked[itemData.uniqueID].goal))
end

--[[------------------------------------------------------
-- AddTracker
-- expects: 
	* itemID - int
	* goal - int
-- returns 
	* nothing
	
-- adds a new token to the tracker, with goal as the goal
-- number of tokens, if goal changes and the item is 
-- already being tracked then the goal is updated.
------------------------------------------------------]]--
function Tokens.AddTracker(itemID, goal)
	if not Tokens.Save.tracked[itemID] then 
		Tokens.Save.tracked[itemID] = {}
		usedTrackers[itemID] = GetTracker(itemID)
		Tokens.Save.tracked[itemID].current, Tokens.Save.tracked[itemID].icon = GetTokenData(itemID)
		Tokens.Save.tracked[itemID].goal = goal
		
		usedTrackers[itemID].Icon:Texture(GetIconData(Tokens.Save.tracked[itemID].icon))
		usedTrackers[itemID].Count:SetText(wstring.format(L"%d/%d", Tokens.Save.tracked[itemID].current, Tokens.Save.tracked[itemID].goal))
		usedTrackers[itemID]:Show()
	elseif Tokens.Save.tracked[itemID].goal ~= goal then
		Tokens.Save.tracked[itemID].goal = goal
		
		usedTrackers[itemID].Count:SetText(wstring.format(L"%d/%d", Tokens.Save.tracked[itemID].current, Tokens.Save.tracked[itemID].goal))
	end
	
	Tokens.OrderTrackers()
end

--[[------------------------------------------------------
-- RemTracker
-- expects:
	* itemID - int
-- returns:
	* nothing
	
-- removes a tracker based on itemID
------------------------------------------------------]]--
function Tokens.RemTracker(itemID)
	if Tokens.Save.tracked[itemID] then
		usedTrackers[itemID]:Hide()
		table.insert(availTrackers, usedTrackers[itemID])
		usedTrackers[itemID] = nil
		Tokens.Save.tracked[itemID] = nil
	end
	
	Tokens.OrderTrackers()
end

--[[------------------------------------------------------
-- OrderTrackers
-- expects:
	* nothing
-- returns:
	* nothing
	
-- attempts to order trackers, and anchor them in a pretty
-- fashion
------------------------------------------------------]]--
function Tokens.OrderTrackers()
	table.sort(usedTrackers)
	
	local prevAnchor = "TokensAnchor"
	for k,v in pairs(usedTrackers) do
		v:ClearAnchors()
		v:AddAnchor(prevAnchor, "bottomleft", "topleft", 0, 0)
		
		prevAnchor = v.name
	end
end

--[[------------------------------------------------------
-- registers slash commands
------------------------------------------------------]]--
function Tokens.RegisterSlash()
	if LibSlash and not LibSlash.IsSlashCmdRegistered("tokens") then
        LibSlash.RegisterWSlashCmd("tokens", Tokens.SlashHandler)
    end
end

--[[------------------------------------------------------
-- creates a new tracker
------------------------------------------------------]]--
function Tokens.SlashHandler(input)
	local newLine = input:gsub(L"<", L"")
	newLine = newLine:gsub(L">", L"")
	local itemID, goal = newLine:match(L'LINK.*data="ITEM:([^"]*)".*text="[%a%s%p]*".*color="[%d%p]*"%D*([%d]*)')
	
	itemID = tonumber(itemID)
	goal = tonumber(goal)
	
	if(not itemID) then
		EA_ChatWindow.Print("[Tokens]: unable to add a new tracker")
		EA_ChatWindow.Print("[Tokens]: [item link] goal - is the proper method to adding a tracker")
	else
		Tokens.AddTracker(itemID, goal)
	end
end